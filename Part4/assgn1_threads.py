import threading ,re
import Queue 

# Store file names in an array 

a = ['all_is_well.txt', 'comedy_of_errors.txt' , 'cymbeline.txt' , 'as_you_like_it.txt' , 'hamlet.txt' , 'anthony_and_cleopatra.txt' , 'coriolanus.txt' , 'king_lear.txt' , 'much_ado.txt' , 'merry_wives.txt' , 'merchant_of_venice.txt' , 'midnight_summer_dream.txt']

List = [{}]*10
dict_temp = [{}]*10
mapper_t=[0]*12
reducer_t =[0]*10
# hash of each word
def hash(str):
	ans = 0
	for i in str:
		ans += ord(i)
	return ans

# mapper function 
def mapper(filename):
	with open(filename, 'r') as f:
		for line in f:
			line = re.sub("[^A-Za-z\s]" , "" , line.strip())
			if len(line) >0:
				words = line.split()
				for word in words:
					word = word.lower()
					bucket = hash(word) % 10
					if word in List[bucket]:
						List[bucket][word] = List[bucket][word] + 1
					else:
						List[bucket][word] = 1

def reducer(i):
	temp_dict={}
	temp_dict = List[i]
	S = open(str('final_thread_' + str(i)), 'w')
	for word in temp_dict:
		S.write( word + " : " + str(temp_dict[word]) +"\n" )
	S.close()

# Initialise threads equal in no. to the texts
for i in xrange(0,12):
	mapper_t[i] = threading.Thread(target = mapper , args = (a[i],))
	mapper_t[i].start()
for i in xrange(0,12):
	mapper_t[i].join()
for i in xrange (0,10):
	reducer_t[i] = threading.Thread(target = reducer , args = (i , ) )
	reducer_t[i].start()
for i in xrange (0,10):
	reducer_t[i].join




