import threading ,re 
import Queue 
from multiprocessing import Process , Lock

# Store file names in an array 

a = ['all_is_well.txt', 'comedy_of_errors.txt' , 'cymbeline.txt' , 'as_you_like_it.txt' , 'hamlet.txt' , 'anthony_and_cleopatra.txt' , 'coriolanus.txt' , 'king_lear.txt' , 'much_ado.txt' , 'merry_wives.txt' , 'merchant_of_venice.txt' , 'midnight_summer_dream.txt']
mapper_t=[0]*12
reducer_t =[0]*10
mapper_pro =[]

# hash of each word
def hash(str):
	ans = 0
	for i in str:
		ans += ord(i)
	return ans

# mapper function 
def mapper(filename,lock):
	f = [0]*10 
	for i in xrange(0,10):
		f[i] = open(str('output'+ str(i)) , 'a')
	with open(filename, 'r') as input_file:
		for line in input_file:
			line = re.sub("[^A-Za-z\s]" , "" , line.strip())
			if len(line) > 0 :
				words = line.split()
				for word in words:
					word = word.lower()
					bucket = hash(word) % 10
					lock[bucket].acquire()
					f[bucket].write(word + " 1\n")
					lock[bucket].release()
	for i in xrange(0,10):
		f[i].close()

def reducer(filename):
	temp_dict={}
	S = open(str('final_' + filename), 'w') 
	with open(filename, 'r') as f:
		for line in f:	
			word = line.strip().split(' ')[0]
			if word in temp_dict:
				temp_dict[word] = temp_dict[word] + 1
			else:
				temp_dict[word] = 1
	for word in temp_dict:
		S.write( word + " : " + str(temp_dict[word]) +"\n" )
	f.close()
	S.close()

# Initialise processes equal in no. to the texts
if __name__ =='__main__':
	l = [0]*10
	for i in xrange(0,10):
		l[i] = Lock()
	for i in xrange(0,12):
		mapper_t[i] = Process(target = mapper , args = (a[i],l))
		mapper_t[i].start()
	for i in xrange(0,12):
		mapper_t[i].join()
	for i in xrange (0,10):
		reducer_t[i] = Process(target = reducer , args = (str('output'+str(i)) , ) )
		reducer_t[i].start()
	for i in xrange (0,10):
		reducer_t[i].join
