#!/usr/bin/env python
from operator import itemgetter
import sys

counter = [0] * 26;

for line in sys.stdin:
	linearr = line.split(' ');
	counter[ord(linearr[0])-97] += int(linearr[1])

f = open('final_output_part1' , 'w')
for i in range(len(counter)):
        f.write(str(chr(97+i) + ' ' + str(counter[i])  + '\n' ))
