#!/usr/bin/env python
from operator import itemgetter
import sys

dict_temp = {};

for line in sys.stdin:
	file_name = line.strip().split(' ')[1]
	word = line.split(' ')[0]
# Storing multiple values (words) for each key(filename) 
	if file_name in dict_temp:
		if word in dict_temp[file_name]:
			x = 'hoohaa!'
		else:
			dict_temp[file_name].append(word)
	else:
		dict_temp[file_name] = [word]

	#counter[ord(linearr[0])-97] += int(linearr[1])

f = open('final_output_part2_2', 'w')
for i in dict_temp:
	if len(dict_temp[i]) == 3:
		f.write(str(i.split('/')[-1] + ' '))
